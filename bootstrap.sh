#!/bin/bash

# make sure that git is installed
apt install git

rm -rf /tmp/ansible
git clone https://vandrham-bootstrap:q5zhfwZB8s2XPDQFVYxV@bitbucket.org/vandrham/ansible.git /tmp/ansible

if [ ! -d "/tmp/ansible" ]; then
  echo "git checkout failed" >&2
  exit 1
fi

apt install ansible
cd /tmp/ansible
./bootstrap.sh
